`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    13:53:12 07/07/2019 
// Design Name: 
// Module Name:    Div_Test 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Div_Test(
    input	Clk,
    input [31:0] A,
    input [31:0] B,
	input	start,
    output [63:0] C
    );
	

	reg [31:0] T;
	wire Complete = ~T[0];
	assign Busy = T[1];
	
	
	reg [63:0] tmpA,tmpB1,tmpB2,tmpB3;
	wire [63:0] sub1 = (tmpA<<2) - tmpB1,
				sub2 = (tmpA<<2) - tmpB2,
				sub3 = (tmpA<<2) - tmpB3;
	initial begin
		T <=0;
		tmpA <= 0;
		tmpB1 <= 0;
		tmpB2 <= 0;
		tmpB3 <= 0;
	end
	
	always @ (posedge Clk) begin
		if(start) begin
			T <= 32'hffffffff;
			tmpA <= {32'b0,A};
			tmpB1 <= {B,32'b0};
			tmpB2 <= {B,32'b0}+{B,32'b0};
			tmpB3 <= {B,33'b0}+{B,32'b0};
		end
		else if(T[15]&&(tmpA[47:16]==32'b0)) begin
			T <= (T>>16);
			tmpA <= (tmpA<<16);
		end
		else if(T[7]&&(tmpA[45:24]==32'b0)) begin
			T <= (T>>8);
			tmpA <= (tmpA<<8);
		end
		else if(T[3]&&(tmpA[59:28]==32'b0)) begin
			T <= (T>>4);
			tmpA <= (tmpA<<4);
		end
		else if(T[0]) begin
			T <= (T>>2);
			tmpA <= (tmpA[63]==sub3[63]) ? sub3 + 3:
					(tmpA[63]==sub2[63]) ? sub2 + 2:
					(tmpA[63]==sub1[63]) ? sub1 + 1:
										   tmpA<<2;
		end
	end
	assign C = tmpA;

endmodule


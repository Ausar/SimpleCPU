`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   13:58:24 07/07/2019
// Design Name:   Div_Test
// Module Name:   D:/LongSonCPU/SimpleCPU/src/DivTB.v
// Project Name:  SimpleCPU
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: Div_Test
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module DivTB;

	// Inputs
	reg Clk;
	reg [31:0] A;
	reg [31:0] B;
	wire [31:0] PosiA = A[31]?-A:A,
				PosiB = B[31]?-B:B;
	
	
	reg start;
	reg sign;
	reg	[1:0] WriteEnable;
	// Outputs
	wire [63:0] C;
	wire [31:0] Hi = C[63:32],
				Lo = C[31:0];
	// Instantiate the Unit Under Test (UUT)
	DivCore uut (
		.Clk(Clk), 
		.A(A), 
		.B(B), 
		.C(C),
		.start(start),
		.sign(sign),
		.Busy(Busy),
		.WriteEnable(WriteEnable)
	);
	
	initial begin
		// Initialize Inputs
		Clk = 0;
		A = 0;
		B = 0;
		WriteEnable = 0;
		// Wait 100 ns for global reset to finish
		#3
		start = 1;
		sign = 0;
		A = 32'h4e775a80;
		B = 32'hb26795ec;
		
		#2		
		start = 0;
		sign = 0;/*
		// Add stimulus here
		#100
		//WriteEnable = 2'b01;
		#10
		WriteEnable = 2'b00;*/
	end
      always #1 Clk = ~Clk;
endmodule


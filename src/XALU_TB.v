`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   11:31:29 07/07/2019
// Design Name:   XALU_Test
// Module Name:   D:/LongSonCPU/SimpleCPU/src/XALU_TB.v
// Project Name:  SimpleCPU
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: XALU_Test
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module XALU_TB;

	// Inputs
	reg Clk;
	reg [31:0] A;
	reg [31:0] B;

	// Outputs
	wire [63:0] C;

	// Instantiate the Unit Under Test (UUT)
	XALU_Test uut (
		.Clk(Clk), 
		.A(A), 
		.B(B), 
		.C(C)
	);

	initial begin
		// Initialize Inputs
		Clk <= 0;
		A <= 0;
		B <= 0;

		// Wait 100 ns for global reset to finish
		#15;
        A <= -5;
		B <= 9;
		#50;
		A <= 6;
		B <= -7;
		#50
		A <= -10;
		B <= -30;
		#50
		A<=7;
		B<=8;
		#50
		A<=0;
		B<=999;
		#50
		A<=999;
		B<=0;
		// Add stimulus here
	end
    always #5 Clk = ~Clk;
endmodule


`timescale 1ns / 1ps
`include "InstrHeader.h"
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2019/07/03 11:16:09
// Design Name: 
// Module Name: ExecuteSdate
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ExecuteSdate(
    input   Clk,
    input   Clr,
	input	[31:0]	D_PC,
    input   [4:0]   D_RsID,
    input   [4:0]   D_RtID,
    input   [31:0]  D_RsData,
    input 	[31:0]  D_RtData,
    input   [4:0]   D_Shamt,
    input   [15:0]  D_Imm16,
    input	[`InstrBusWidth]  D_InstrBus,
    input 	[3:0]   D_T,
    input			D_WriteRegEnable,
    input	[4:0]   D_RegId,
    
    input           M_WriteRegEnable,
    input   [4:0]   M_RegId,
    input   [31:0]  M_Data,

	output reg	[31:0]	E_PC,
    output reg [31:0]   E_WriteMemData,
    output reg [4:0]    E_RtID,

    output reg	[3:0]   E_T,
    output reg			E_WriteRegEnable,
    output reg	[4:0]	E_RegId,
    output reg	[31:0]  E_Data,

    output reg	[4:0]   E_ExtType,
    output reg	[3:0]   E_MemWriteEnable,
    output reg	E_MemFamily,
	output	E_XALU_Busy
    );
	wire	`InstrBusWireSet;
	assign	{`InstrBusWireSet}	=	D_InstrBus;
    /////////////////////转发////////////////
    wire    [31:0]  MF_Rs = (D_RsID!=0 && D_RsID==E_RegId && E_T==0 && E_WriteRegEnable)		?   E_Data:
                            (D_RsID!=0 && M_WriteRegEnable && M_RegId==D_RsID)								?   M_Data:
                                                                                                    D_RsData;
    wire    [31:0]  MF_Rt = (D_RtID!=0 && D_RtID==E_RegId && E_T==0 && E_WriteRegEnable)		?	E_Data:
                            (D_RtID!=0 && M_WriteRegEnable && M_RegId==D_RtID)								?   M_Data://M级结束（实际上是W级了），那么T必定�
                                                                                                    D_RtData;
    ////////////////////////////////////////
    wire    [31:0]  C_Inter;
    ALU ALU(.A(MF_Rs),.B(MF_Rt),.shamt(D_Shamt),.Imm16(D_Imm16),.InstrBus(D_InstrBus),.C(C_Inter),.PC(D_PC));

	/*module XALU(
    input Clk,
    input Clr,
    input [`InstrBusWidth] InstrBus,
	
    input [31:0] XALU_A,
    input [31:0] XALU_B,
    output reg [31:0] XALU_HI,
    output reg [31:0] XALU_LO,
	
	input XALU_Start,
    output XALU_Busy
    );*/
	wire [31:0] XALU_HI,
				XALU_LO;
	wire		XALU_Busy_Inter;
	
	XALU XALU(.Clk(Clk),.Clr(Clr),.InstrBus(D_InstrBus),.XALU_A(MF_Rs),.XALU_B(MF_Rt),.XALU_HI(XALU_HI),.XALU_LO(XALU_LO),.XALU_Busy(XALU_Busy_Inter));


    wire    [4:0]   ExtType_Inter;
    wire    [3:0]   MemWriteEnable_Inter;
    wire    MemFamily_Inter;
	wire	[1:0]	Offset = C_Inter[1:0];
    MemCtrlUnit MemCtrlUnit(.InstrBus(D_InstrBus),.Offset(Offset),.ExtType(ExtType_Inter),.MemWriteEnable(MemWriteEnable_Inter),.MemFamily(MemFamily_Inter));
    //////////////////////////////////////////////////////////////////////////////////////////
    wire    [3:0] E_T_Inter = (D_T > 0)?D_T-1:0;
	wire	[31:0]	Data_Inter = 	mfhi	?	XALU_HI:
									mflo	?	XALU_LO:
												C_Inter;
	initial begin
		E_PC <= 0;
		E_WriteMemData <= 0;
		E_RtID <= 0;
		E_T <= 0;
		E_WriteRegEnable <= 0;
		E_RegId <= 0;
		E_Data <= 0;
		E_ExtType <= 0;
		E_MemWriteEnable <= 0;
		E_MemFamily <= 0;
		//E_XALU_Busy <= 0;
	end
    
    always @ (posedge Clk) begin
        if(Clr) begin
			E_PC <= 0;
			E_WriteMemData <= 0;
			E_RtID <= 0;
			E_T <= 0;
			E_WriteRegEnable <= 0;
			E_RegId <= 0;
			E_Data <= 0;
			E_ExtType <= 0;
			E_MemWriteEnable <= 0;
			E_MemFamily <= 0;
			//E_XALU_Busy <= 0;
		end
		else begin
			E_PC <= D_PC;
			E_WriteMemData <= (MF_Rt<<({Offset,3'b0}));
			E_RtID <= D_RtID;
			E_T <= D_T ==4'b0 ? 4'b0 : D_T-1;
			E_WriteRegEnable <= D_WriteRegEnable;
			E_RegId <= D_RegId;
			E_Data <= Data_Inter;
			E_ExtType <= ExtType_Inter;
			E_MemWriteEnable <= MemWriteEnable_Inter;
			E_MemFamily <= MemFamily_Inter;
		end
		//E_XALU_Busy <= Clr ? 0:XALU_Busy_Inter;
		
    end
		assign E_XALU_Busy = XALU_Busy_Inter;

endmodule

`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2019/07/03 15:52:50
// Design Name: 
// Module Name: MemState
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

//注意，这一个模块里面实际上把WriteBack级给包括进来�//在后续扩展中，很可能会在这个模块中加入内部流水级,也就是说，很可能这个模块是二级流�
module MemState(
        input   Clk,
        input   Clr,
		input	[31:0]	E_PC,
        input   [31:0]  E_MemWriteData,
        input   [4:0]   E_RtID,
        input   [31:0]  E_Data,
        input   [4:0]   E_ExtType,
        input   [3:0]   E_MemWriteEnable,
        input           E_WriteRegEnable,
        input   [4:0]   E_RegId,
        input   		E_MemFamily,

        output reg			M_WriteRegEnable,
        output reg	[4:0]   M_RegId,
        output 		[31:0]  M_Data,
		output reg	[31:0]	M_PC
    );
    /*module DataMem(
    input   Clk,
	input	[31:0]	PC,
    input   [31:0]  DataAddr,
    input   [31:0]  WriteData,
    input   [3:0]   WriteEnble,
    output  [31:0]  ReadData
    );*/
    ///////////////////转发//////////////////////////////
    wire    [31:0]  MF_Rt = (M_WriteRegEnable && M_RegId==E_RtID)				?   M_Data:
																					E_MemWriteData;
    ////////////////////////////////////////////////////
    wire [31:0] MemReadData_Inter;
    DataMem DataMem(.Clk(Clk),.DataAddr(E_Data),.PC(E_PC),.WriteData(MF_Rt),.WriteEnble(E_MemWriteEnable),.ReadData(MemReadData_Inter));

    
    
    ////////////////////////////
    wire [31:0] Ans = E_MemFamily ? MemReadData_Inter:E_Data;
    wire [1:0]  Offset_Inter = E_Data[1:0];


    reg [1:0] M_Offset;
    reg [4:0] M_ExtType;
    reg [31:0] M_RawData;
	reg		M_MemFamily;
	initial begin
		M_WriteRegEnable <= 0;
		M_RegId <= 0;
		M_Offset <= 0;
		M_ExtType <= 0;
		M_RawData <= 0;
		M_MemFamily <= 0;
		M_PC <= 0;
	end
    always @ (posedge Clk) begin
		if(Clr) begin
			M_WriteRegEnable <= 0;
			M_RegId <= 0;
			M_Offset <= 0;
			M_ExtType <= 0;
			M_RawData <= 0;
			M_MemFamily <= 0;
			M_PC <= 0;
		end
		else begin
			M_WriteRegEnable <= E_WriteRegEnable;
			M_RegId <= E_RegId;
			M_Offset <= Offset_Inter;
			M_ExtType <= E_ExtType;
			M_RawData <= Ans;
			M_MemFamily <= E_MemFamily;
			M_PC <= E_PC;
		end
    end
    //////////////////////////////////////
	//W��
	
    /*module MemExtUnit(
    input   [31:0]  RawMemData,
    input   [1:0]   Offset,
    input   [4:0]   ExtType,
    output  [31:0]  ExtMemData
    );*/
	wire [31:0] ExtMemData;
    MemExtUnit MemExtUnit(.RawMemData(M_RawData),.Offset(M_Offset),.ExtType(M_ExtType),.ExtMemData(ExtMemData));
    assign M_Data = M_MemFamily ? ExtMemData:M_RawData;
endmodule

`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    21:33:37 06/26/2019 
// Design Name: 
// Module Name:    InstrFetchUnit 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
`define TextAddr 32'h3000
//`define TextAddr 32'hbfc00000
module InstrFetchUnit(
    input Clk,
    input Clr,
	input stall,
	input [31:0]NewPcAddr,
	output reg	[31:0]	PC,
    output 	[31:0] Instr
    );
	InstrMem rom(PC,Instr);
	/////////////////////////////////////
	initial begin
		PC<= `TextAddr;
	end
	/////////////////////////////////////
	
	 
	 
	always @ (posedge Clk)begin
		if(Clr) begin
			PC<=`TextAddr;
		end
		else if(stall) begin
			PC<=PC;
		end
		else begin//正常的情况下
			PC<=NewPcAddr;
		end
	end

endmodule

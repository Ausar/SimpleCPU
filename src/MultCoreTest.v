`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   21:00:46 07/08/2019
// Design Name:   MultCore
// Module Name:   D:/LongSonCPU/SimpleCPU/src/MultCoreTest.v
// Project Name:  SimpleCPU
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: MultCore
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module MultCoreTest;

	// Inputs
	reg Clk;
	reg [31:0] A;
	reg [31:0] B;
	reg start;
	reg sign;
	wire [31:0] PosiA = A[31]?-A:A,
				PosiB = B[31]?-B:B;
	// Outputs
	wire [31:0] HI;
	wire [31:0] LO;
	wire Busy;
	reg [1:0] WriteEnable;

	// Instantiate the Unit Under Test (UUT)
	MultCore uut (
		.Clk(Clk), 
		.A(A), 
		.B(B), 
		.HI(HI),
		.LO(LO),
		.sign(sign),
		.start(start), 
		.WriteEnable(WriteEnable),
		.Busy(Busy)
	);

	initial begin
		// Initialize Inputs
		Clk = 0;
		A = 0;
		B = 0;
		start = 0;
		WriteEnable = 0;
		// Wait 100 ns for global reset to finish
		#15;
		A = -1000010;
		B = 500002333;
		sign = 1;
		start = 1;
		#10
		start = 0;
		
		/*start = 1;
		#5
		A = 0;
		B = 0;
		start = 0;
        #100;
		A = 999;
		B = 5;
		start = 1;
		#10
		A = 0;
		B = 0;
		start = 0;
		// Add stimulus here*/
	
	end
    always #5 Clk=~Clk;  
endmodule


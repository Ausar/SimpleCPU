`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2019/07/02 10:19:05
// Design Name: 
// Module Name: InstrFetchState
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

`define TextAddr 32'h3000
//`define TextAddr 32'hbfc00000
module InstrFetchState(
    input   Clk,
    input   Clr,
    input   		D_stall_Pass,
    input   [31:0]  D_NewPC_Pass,
    output reg	[31:0]	I_PC,
	output		[31:0]	I_PC_Pass,
    output reg	[31:0] 	I_Instr
    );
    /*
    module InstrFetchUnit(
    input Clk,
    input Clr,
	input stall,
	input [31:0]NewPcAddr,
	output	reg	[31:0]	PC,
    output	[31:0] Instr
    );
    */
    wire [31:0] PC_Inter,Instr_Inter;
	assign I_PC_Pass = PC_Inter;
    InstrFetchUnit  IFU(.Clk(Clk),.Clr(Clr),.stall(D_stall_Pass),.NewPcAddr(D_NewPC_Pass),.PC(PC_Inter),.Instr(Instr_Inter));
    
    //////////////////////////////////////////////////////////////////////////////
	initial begin
		I_PC <= 0;
		I_Instr <= 0;
	end
    always  @   (posedge Clk)   begin
		if(Clr) begin
			I_PC <= 0;
			I_Instr <= 0;
		end
		else if(!D_stall_Pass) begin
			I_PC <= PC_Inter;
			I_Instr <= Instr_Inter;
		end
    end
    ///////////////////////////////////////////////////////////////////////////////
endmodule

`timescale 1ns / 1ps
`include "InstrHeader.h"
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    21:43:26 06/26/2019 
// Design Name: 
// Module Name:    TopLevel 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module TopLevel(
	input	Clk,
	input	Clr
    );
	

	wire D_stall_Pass;
	wire [31:0] D_NewPC_Pass,I_PC_Pass;
	
	wire [31:0] I_PC,I_Instr;
	
	InstrFetchState InstrFetchState(.Clk(Clk),.Clr(Clr),.D_stall_Pass(D_stall_Pass),.D_NewPC_Pass(D_NewPC_Pass),.I_PC(I_PC),.I_PC_Pass(I_PC_Pass),.I_Instr(I_Instr));
	

	wire M_WriteRegEnable;
	wire [4:0] M_RegWriteId;
	wire [31:0] M_RegWriteData;
	wire [4:0] D_RsID,D_RtID;
	wire [31:0] D_RsData,D_RtData;
	wire [4:0] D_Shamt;
	wire [15:0] D_Imm16;
	wire [`InstrBusWidth] D_InstrBus;
	wire [31:0]	D_PC,M_PC;
	wire [3:0] E_T,D_T;
	wire E_WriteRegEnable,D_WriteRegEnable;
	wire [4:0] E_RegId,D_RegId;
	wire [31:0] E_Data;
	wire E_XALU_Busy;
	DecodeState DecodeState(.Clk(Clk),.Clr(Clr),.W_WriteRegEnable(M_WriteRegEnable),.W_RegWriteId(M_RegId),.W_RegWriteData(M_Data),.I_PC_Pass(I_PC_Pass),.I_PC(I_PC),.I_MipsInstr(I_Instr),.W_PC(M_PC),
							.D_NewPC_Pass(D_NewPC_Pass),.D_PC(D_PC),.D_RsID(D_RsID),.D_RtID(D_RtID),.D_RsData(D_RsData),.D_RtData(D_RtData),.D_Shamt(D_Shamt),.D_Imm16(D_Imm16),.D_InstrBus(D_InstrBus),
							.E_T(E_T),.E_WriteRegEnable(E_WriteRegEnable),.E_RegId(E_RegId),.E_Data(E_Data),.D_T(D_T),.D_WriteRegEnable(D_WriteRegEnable),.D_RegId(D_RegId),.D_stall_Pass(D_stall_Pass),
							.E_XALU_Busy(E_XALU_Busy));
	

	wire [4:0] M_RegId;
	wire [31:0] M_Data;
	wire [31:0] E_PC;
	wire [31:0] E_WriteMemData;
	wire [4:0] E_RtID;
	wire [3:0] E_MemWriteEnable;
	wire E_MemFamily;
	wire [4:0] E_ExtType;
	ExecuteSdate ExecuteSdate(.Clk(Clk),.Clr(Clr),.D_PC(D_PC),.D_RsID(D_RsID),.D_RtID(D_RtID),.D_RsData(D_RsData),.D_RtData(D_RtData),.D_Shamt(D_Shamt),.D_Imm16(D_Imm16),.D_InstrBus(D_InstrBus),.D_T(D_T),.D_WriteRegEnable(D_WriteRegEnable),
								.D_RegId(D_RegId),.M_WriteRegEnable(M_WriteRegEnable),.M_RegId(M_RegId),.M_Data(M_Data),.E_PC(E_PC),.E_WriteMemData(E_WriteMemData),.E_RtID(E_RtID),
								.E_T(E_T),.E_WriteRegEnable(E_WriteRegEnable),.E_RegId(E_RegId),.E_Data(E_Data),.E_ExtType(E_ExtType),
								.E_MemWriteEnable(E_MemWriteEnable),.E_MemFamily(E_MemFamily),
								.E_XALU_Busy(E_XALU_Busy));

	MemState MemState(.Clk(Clk),.Clr(Clr),.E_PC(E_PC),.E_MemWriteData(E_WriteMemData),.E_RtID(E_RtID),.E_Data(E_Data),.E_ExtType(E_ExtType),.E_MemWriteEnable(E_MemWriteEnable),.E_WriteRegEnable(E_WriteRegEnable),
						.E_RegId(E_RegId),.E_MemFamily(E_MemFamily),.M_WriteRegEnable(M_WriteRegEnable),.M_RegId(M_RegId),.M_Data(M_Data),.M_PC(M_PC));
endmodule

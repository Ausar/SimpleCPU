`timescale 1ns / 1ps
`include "InstrHeader.h"
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2019/07/03 14:31:46
// Design Name: 
// Module Name: MemCtrlUnit
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module MemCtrlUnit(
    input   [`InstrBusWidth]  InstrBus,
    input   [1:0]   Offset,//地址末尾的两�    
	output  [4:0]   ExtType,
    output  [3:0]   MemWriteEnable,
    output  MemFamily
    );
    
	assign	{`InstrBusWireSet}	=	InstrBus;
    assign	ExtType = {lb,lbu,lh,lhu,lw};
    assign  MemFamily = lb|lbu|lh|lhu|lw|sb|sh|sw;
    assign  MemWriteEnable  =   sw  ?   4'b1111:
                                sh  ?   4'b0011<<Offset:
                                sb  ?   4'b0001<<Offset:
                                        4'b0000;
    
    
endmodule
